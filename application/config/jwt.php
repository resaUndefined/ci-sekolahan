<?php

defined('BASEPATH') OR exit('No direct script access allowed');

$config['jwt_key'] = 'e248ed31d947688070a6472b692983eda78884064ca1bdf1819cd4a25a16d70a';
$config['jwt_algorithm'] = 'HS256';
$config['jwt_issuer'] = 'https://serverprovider.com';
$config['jwt_audience'] = 'https://serverclient.com';
$config['jwt_expire'] = 3600;


