<?php
    class M_Kontak extends CI_Model
    {
        function fetch_all()
        {
            $this->db->order_by('id', 'ASC');
            $query = $this->db->get('telepon');

            return $query->result_array();
        }

        function fetch_single_data($id)
        {
            $this->db->where("id", $id);
            $query = $this->db->get('telepon');
                
            return $query->row();
        }

        function check_data($id)
        {
            $this->db->where("id", $id);
            $query = $this->db->get('telepon');
                
            if ($query->row()) {
                return true;
            } else {
                return false;
            }
            
        }

        function insert_api($param)
        {
            $this->db->insert('telepon', $param);
            if ($this->db->affected_rows() > 0){
                return true;
            } else {
                return false;
            }
        }                

        function update_data($param1, $param2)
        {
            $this->db->where("id", $param1);
            $this->db->update("telepon", $param2);
        }

        function delete_data($paramId)
        {
            $this->db->where("id", $paramId);
            $this->db->delete("telepon");
            if ($this->db->affected_rows() > 0) {
                return true;
            } else {
                return false;
            }
        }

        function panggil_nama($nama)
        {
            return "Hallo, nama saya adalah ".$nama;
        }
    }
?>