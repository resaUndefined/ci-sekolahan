<?php

    defined('BASEPATH') OR exit('No direct script access allowed');

    require APPPATH . '/libraries/REST_Controller.php';
    require_once FCPATH . 'vendor/autoload.php';

    use Restserver\Libraries\REST_Controller;

    class Kontak extends REST_Controller {

        function __construct($config = 'rest') {
            parent::__construct($config);
            $this->load->database(); //optional
            $this->load->model('M_Kontak');
            $this->load->library('form_validation');
            $this->load->library('jwt');
        }            

        function index_get()
        {
            if ($this->jwt->decode($this->input->request_headers()['Authorization']) == false){                
                return $this->response(
                                        array(
                                                'kode'=>'401', 
                                                'pesan'=>'signature tidak sesuai', 
                                                'data'=>[]
                                            ), '401'
                                        );                
            }        
            $id = $this->get('id');
            if ($id == '') {
                $data = $this->M_Kontak->fetch_all();
            } else {            
                $data = $this->M_Kontak->fetch_single_data($id);
            }                

            $this->response($data, 200);
        }

        function index_post()
        {
            if ($this->jwt->decode($this->input->request_headers()['Authorization']) == false){                
                return $this->response(
                                        array(
                                                'kode'=>'401', 
                                                'pesan'=>'signature tidak sesuai', 
                                                'data'=>[]
                                            ), '401'
                                        );                
            }

            if ($this->post('nama_inputan') == '') {
                $response = array(
                    'status' => 'fail',
                    'field' => 'nama',
                    'message' => 'Isian nama tidak boleh kosong!',
                    'status_code' => 502
                );

                return $this->response($response);
            }

            if ($this->post('nomor_attr') == '') {
                $response = array(
                    'status' => 'fail',
                    'field' => 'nomor',
                    'message' => 'Isian nomor tidak boleh kosong!',
                    'status_code' => 502
                );            

                return $this->response($response);
            }

            $data = array(
                'name' => trim($this->post('nama_inputan')),
                'number'  => trim($this->post('nomor_attr'))
            );            

            $this->M_Kontak->insert_api($data); //memanggil fungsi insert data yg ada di model

            $last_row = $this->db->select('*')->order_by('id',"desc")->limit(1)->get('telepon')->row();
            $response = array(
                'status' => 'success',
                'data' => $last_row,
                'status_code' => 201,
            );

            return $this->response($response);        
        }

        function index_put()
        {    
            $id = $this->put('id');    
            $check = $this->M_Kontak->check_data($id);        
            if ($check == false) {
                $error = array(
                    'status' => 'fail',
                    'field' => 'id',
                    'message' => 'Data tidak ditemukan!',
                    'status_code' => 404
                );

                return $this->response($error);
            }
            if ($this->put('nama_inputan') == '') {
                $response = array(
                    'status' => 'fail',
                    'field' => 'nama',
                    'message' => 'Isian nama tidak boleh kosong!',
                    'status_code' => 502
                );

                return $this->response($response);
            }

            if ($this->put('nomor_inputan') == '') {
                $response = array(
                    'status' => 'fail',
                    'field' => 'nomor',
                    'message' => 'Isian nomor tidak boleh kosong!',
                    'status_code' => 502
                );            

                return $this->response($response);
            }
            $data = array(
                'name' => trim($this->put('nama_inputan')),
                'number'  => trim($this->put('nomor_inputan'))
            );

            $this->M_Kontak->update_data($id,$data);
            $newData = $this->M_Kontak->fetch_single_data($id);            

            return $this->response(responseSuccess('success',$newData,200));
        }

        function index_delete() {
            $id = $this->delete('id');    
            $check = $this->M_Kontak->check_data($id);                
            if ($check == false) {
                $error = array(
                    'status' => 'fail',
                    'field' => 'id',
                    'message' => 'Data tidak ditemukan!',
                    'status_code' => 502
                );

                return $this->response($error);
            }
            $delete = $this->M_Kontak->delete_data($id);            

            return $this->response(responseSuccess('success',null,200));
        }

        
        private function responseSuccess($param1,$param2,$param3)
        {
            $data = array(
                'status' => $param1,
                'data' => $param2,
                'status_code' => $param3,
            );

            return $data;
        }
    }
?>